from fastapi import APIRouter, HTTPException
from models import Extracted, Data, Page, Source
from pydantic import AnyHttpUrl
from config import get_logger
from extractor import extract_article


router = APIRouter()
logger = get_logger()


@router.get("/extract", response_model=Extracted, response_model_exclude_none=True)
async def extract_article_from_url(url: AnyHttpUrl):
    try:
        return await extract_article(url)
    except Exception as e:
        logger.error(f"Error in article extraction: {e}")
        raise HTTPException(
            status_code=422, detail="Could extract content from the url."
        )
