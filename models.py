from pydantic import BaseModel, AnyHttpUrl, Field
from datetime import datetime
from typing import List


class Data(BaseModel):
    language: str
    nationality: str = "xx"
    source: AnyHttpUrl
    date: datetime = datetime.now()
    title: str
    description: str
    text: str
    image: AnyHttpUrl
    keywords: list = None


class Page(BaseModel):
    url: AnyHttpUrl
    level: int = 0


class Source(BaseModel):
    nationality: str = "xx"
    source: AnyHttpUrl


class Extracted(BaseModel):
    data: Data
    page: Page
    source: Source
