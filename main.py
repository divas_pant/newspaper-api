import os
from time import time
import requests
import newspaper
import router
from fastapi import FastAPI, Request
import uvicorn
from config import get_logger, get_settings


logger = get_logger()
app = FastAPI()
app.include_router(router.router)


@app.get("/")
def homepage():
    return "Py Scraper"


@app.middleware("http")
async def evaluate_process_time_and_log(request: Request, call_next):
    logger.info(f"Incoming request on {request.url.path} from {request.client.host}")
    start = time()
    response = await call_next(request)
    eval_time = time() - start
    logger.info(f"Request from {request.client.host} completed in {eval_time}")
    # if response.status_code in range(500, 599):
    #     send_error_email(500, f"Server error for request.")
    return response


if __name__ == "__main__":
    settings = get_settings()
    logger.info(f"Server running on port {settings.PORT}")
    uvicorn.run("main:app", host=settings.HOST, port=settings.PORT)
