from newspaper import Article
from config import get_logger
from models import Extracted, Data, Page, Source


logger = get_logger()


async def extract_article(url):
    article = Article(url)
    try:
        article.download()
    except Exception as e:
        logger.error(f"Error in downloading article. {e}")
        raise HTTPException(
            status_code=422, detail="Could not extract content from article."
        )
    try:
        article.parse()
    except Exception as e:
        logger.error(f"Error in parsing article. {e}")
        raise HTTPException(
            status_code=422, detail="Could not extract content from article."
        )

    data = Data(
        language=article.meta_lang,
        date=article.publish_date,
        source=article.source_url,
        title=article.title,
        text=article.text,
        image=article.meta_img,
        description=article.meta_description,
        keywords=article.keywords,
    )

    page = Page(url=article.url)
    source = Source(source=article.source_url)

    return Extracted(data=data.dict(), page=page.dict(), source=source.dict())
