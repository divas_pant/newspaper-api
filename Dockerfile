FROM python:3.8.13

WORKDIR /Repo

COPY . .

RUN pip install pipenv

RUN pipenv install --deploy

CMD pipenv run python main.py
