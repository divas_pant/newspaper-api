import os
import logging
from pydantic import BaseSettings
import logging.config


class Settings(BaseSettings):
    HOST: str = "0.0.0.0"
    PORT: int = 8888


def get_settings():
    return Settings()


def get_logger():
    logging.config.fileConfig("logger.conf", disable_existing_loggers=True)
    if os.environ.get("ENV", None) == "prod":
        return logging.getLogger("Newspaper-API")
    else:
        logger = logging.getLogger("Newspaper-API:Dev")
        logger.propagate = False
        return logger
