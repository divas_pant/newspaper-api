# Python Scraper API #

API based on Newspaper3k library with fast API interface.

### Setup ###
The project is tested on python 3.8 and above with Pipenv used as dependency manager.

To Run project, set up clone project and setup virtual environment using Pipenv.
